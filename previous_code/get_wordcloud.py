
# coding: utf-8

# In[33]:


# reference : https://financedata.github.io/posts/python_news_cloud_text.html

from konlpy.tag import Mecab
import shutil
from os import listdir
from os.path import isfile, join
from collections import Counter
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
from wordcloud import WordCloud
import copy


# In[36]:


f = open('/Users/yangseongjun/Desktop/keyword_list2.txt','r')
lines = f.readlines()
f.close()

lines = lines[0].split()
after_list = copy.copy(lines)

for i in range(len(lines)):
    if(lines[i] == '홀수형'):
        after_list.remove('홀수형')

#lines.remove('홀수형')


# In[40]:


count = Counter(after_list)
tags = count.most_common(100)


# In[41]:


font_path = '/usr/share/fonts/truetype/nanum/NanumMyeongjoBold.ttf'

wc = WordCloud(font_path=font_path, background_color='white', width=800, height=600)
cloud = wc.generate_from_frequencies(dict(tags))
plt.figure(figsize=(10,8))
plt.axis('off')
plt.imshow(cloud)

