
# coding: utf-8

# In[24]:


from navernewscrawler.naver_crawler import get_news, get_dates, output
import re
import shutil
from os import listdir
from os.path import isfile, join

from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from tqdm import tqdm

import ssl
ssl._create_default_https_context = ssl._create_unverified_context

stopWords = stopwords.words('english')

docLabels = []
docLabels = [f for f in listdir("result/topic") if f.endswith('.txt')]

topic_keywords = []


# In[25]:


def get_keywords(source):
    f = open('result/topic/' + source,'r')
    lines = f.readlines()
    f.close()
    lines = ''.join(lines)
    topic_keywords = lines.split()
    return topic_keywords;
    
for i in range(len(docLabels)):
    topic_keywords.append(get_keywords(docLabels[i]))


# In[26]:


news_data = []


# In[27]:


def get_news_data(query):
    news_url = "https://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=102&oid=001&aid=0010706739"
    news_result = get_news(news_url)
    search_result = output(query=query, page=1, max_page=10, start_date="2019-05-01",end_date="2019-05-31")
    return search_result;

## 키워드 3개를 각 뉴스기사에 매칭하여 긁어오는 반복문
#for i in tqdm(range(len(topic_keywords))):
#    for j in tqdm(range(len(topic_keywords[i]) - 2)):
#        query = topic_keywords[i][j] + " " + topic_keywords[i][j+1] + " " + topic_keywords[i][j+2]
#        news_data = news_data + get_news_data(query)


# 토픽0에서 임의이 3개의 키워드를 매칭하여 기사를 긁어오는 코드
query = topic_keywords[0][20] + " " + topic_keywords[0][21] + " " + topic_keywords[0][22]
news_data = news_data + get_news_data(query)

# 토픽1에서 임의이 3개의 키워드를 매칭하여 기사를 긁어오는 코드
query = topic_keywords[1][20] + " " + topic_keywords[1][21] + " " + topic_keywords[1][22]
news_data = news_data + get_news_data(query)

# 토픽2에서 임의이 3개의 키워드를 매칭하여 기사를 긁어오는 코드
query = topic_keywords[2][20] + " " + topic_keywords[2][21] + " " + topic_keywords[2][22]
news_data = news_data + get_news_data(query)

# 토픽3에서 임의이 3개의 키워드를 매칭하여 기사를 긁어오는 코드
query = topic_keywords[3][20] + " " + topic_keywords[3][21] + " " + topic_keywords[3][22]
news_data = news_data + get_news_data(query)

# 토픽4에서 임의이 3개의 키워드를 매칭하여 기사를 긁어오는 코드
query = topic_keywords[4][20] + " " + topic_keywords[4][21] + " " + topic_keywords[4][22]
news_data = news_data + get_news_data(query)

# 토픽5에서 임의이 3개의 키워드를 매칭하여 기사를 긁어오는 코드
query = topic_keywords[5][20] + " " + topic_keywords[5][21] + " " + topic_keywords[5][22]
news_data = news_data + get_news_data(query)

# 토픽6에서 임의이 3개의 키워드를 매칭하여 기사를 긁어오는 코드
query = topic_keywords[6][20] + " " + topic_keywords[6][21] + " " + topic_keywords[6][22]
news_data = news_data + get_news_data(query)


# In[28]:


import copy

if len(news_data) > 0:
    preprocessed = list({search['title']: search for search in news_data}.values())
    preprocessed_copy = copy.copy(preprocessed) 
    train_set = []
    
    for i in range(len(preprocessed)):
        train_set.append(preprocessed[i]['text'])
        
        
    tfidf_vectorizer = TfidfVectorizer(stop_words=stopWords)
    tfidf_matrix_train = tfidf_vectorizer.fit_transform(train_set)

    compare_news = cosine_similarity(tfidf_matrix_train, tfidf_matrix_train)

    for i in range(len(compare_news) - 1):
        for j in range(i+1,len(compare_news)):
            if compare_news[i][j] > 0.5 :
                try:
                    preprocessed_copy.remove(preprocessed[i])
                except:
                    print("값이 비어있습니다")
            
    f = open('news.txt','w')
    for i in range(len(preprocessed_copy)):
        f.write("%s " % preprocessed_copy[i]['title'])
        f.write(" | %s " % preprocessed_copy[i]['company'])
        f.write("\n")
        f.write("%s " % preprocessed_copy[i]['text'])
        f.write('\n\n')
    f.close()    
    


# In[23]:


f = open('news_link_list.txt','w')
for i in range(len(preprocessed_copy)):
    title_str = '[Title] : ' + preprocessed_copy[i]['title'] + '  | [Company] : ' + preprocessed_copy[i]['company'] + '\n'
    f.write(title_str)
f.close()

