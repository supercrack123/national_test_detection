
# coding: utf-8

# In[1]:


# 실시간 검색어 크롤링
import requests
from bs4 import BeautifulSoup
import datetime

s = datetime.datetime.now()
target = s.strftime('%Y-%m-%d')

# Naver 실시간 검색어 API
req = requests.get('https://www.naver.com/')
soup = BeautifulSoup(req.content, 'html.parser')


title_list = soup.select('.ah_list .ah_k') 

f = open('Realtime_keyword'+target+'.txt','a')
        
data = ""

for i, t in enumerate(title_list,1): #title_list에 숫자를 매긴다. (1부터시작함.)
    data = data + " " + t.get_text()
    f.write(t.get_text()+'\n')

    
    
# Naver 실시간 뉴스토픽 API    
html = requests.get('https://search.naver.com/search.naver?sm=tab_hty.top&where=post&query=ro0opf&oquery=a&tqi=Uc%2BgmspySDlsscImflGssssssid-378122').content
soup = BeautifulSoup(html, 'html.parser') 

title_list2 = [] 

for realtime in soup.find(class_='lst_realtime_srch _tab_area').find_all('li'): 
    tg2 = realtime.find(class_='tit') 
    title_list2.append(tg2.text)

for i, t in enumerate(title_list,1):
    data = data + " " + t.get_text()
    f.write(t.get_text()+'\n')

for i, t in enumerate(title_list,11):
    data = data + " " + t.get_text()
    f.write(t.get_text()+'\n')

    
# Daum 실시간 검색어 API    
html = requests.get('https://www.daum.net/').text
soup = BeautifulSoup(html, 'html.parser')

title_list = soup.select('.hotissue_mini a[class*=link_issue]')
ranking = soup.select('.list_mini span[class*=ir_wa]')

for rank,title in zip(ranking , title_list):
    r = ''.join(rank)
    t = ''.join(title)
    f.write(t + '\n')

f.close()

