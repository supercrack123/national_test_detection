
# coding: utf-8

# In[8]:


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups
from nltk.corpus import names
from nltk.stem import WordNetLemmatizer
from sklearn.decomposition import NMF
from sklearn.decomposition import LatentDirichletAllocation


# In[9]:


f = open('keyword_list.txt','r')
keyword_list = f.readlines()
f.close()


# In[12]:


cv = CountVectorizer(stop_words="english", max_features=300)
transformed = cv.fit_transform(keyword_list)
tf_feature_names = cv.get_feature_names()


lda = LatentDirichletAllocation(n_components=7, max_iter=20000, random_state=45, batch_size =150).fit(transformed)

for topic_idx, topic in enumerate(lda.components_):
    label = '{}'.format(topic_idx)
    f = open('topic_result'+label+'.txt','w')
    f.write("%s " % " ".join([cv.get_feature_names()[i] for i in topic.argsort()[::-1]]))
    f.close()

