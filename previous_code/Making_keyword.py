
# coding: utf-8

# In[1]:


import re
import shutil
from os import listdir
from os.path import isfile, join

from konlpy.corpus import kolaw
from konlpy.tag import Twitter

from konlpy.utils import pprint


# In[2]:


# 헌법 조항 데이터 가져온 후, Twitter Tag로 전처리

fids = kolaw.fileids()
fobj = kolaw.open(fids[0])

law_data = ""
law_data = law_data + fobj.read()

twitter = Twitter()
law_keyword = twitter.nouns(law_data)


# In[3]:


# LEET 지문 텍스트 데이터 불러오기
docLabels = []
docLabels = [f for f in listdir("result/언어이해_텍스트") if f.endswith('.txt')]
test1 = []
test1_data = ""

docLabels2 = []
docLabels2 = [f for f in listdir("result/추리논증_텍스트") if f.endswith('.txt')]
test2 = []
test2_data = ""


# In[4]:


# LEET 지문 데이터를 불러온 후, Twitter Tag로 전처리

def all_process(source):
    f = open('result/' + source,'r')
    lines = f.readlines()
    f.close()
    return lines;
    
    
for i in range(len(docLabels)):
    test1 = test1 + all_process('언어이해_텍스트/' + docLabels[i])

test1_data = "".join(test1)

for i in range(len(docLabels2)):
    test2 = test2 + all_process('추리논증_텍스트/' + docLabels2[i])

test2_data = "".join(test2)


total_test_data = test1_data + test2_data

test_keyword = twitter.nouns(total_test_data) # LEET 지문 데이터 전처리 이후 키워드 추출


# In[5]:


# 실시간 검색어 데이터 불러오기
docLabels3 = []
docLabels3 = [f for f in listdir("result/Realtime_keyword") if f.endswith('.txt')]
Realtime = []
Realtime_keyword = ""

def all_process2(source):
    f = open(source,'r', encoding='utf-8',  errors='ignore')
    lines = f.readlines()
    f.close()
    return lines;
    
for i in range(len(docLabels3)):
    Realtime = Realtime + all_process2('result/Realtime_keyword/' + docLabels3[i])
    
Realtime_keyword = "".join(Realtime)
Realtime_keyword = twitter.nouns(Realtime_keyword) # 실시간 검색어 데이터를 모두 합쳐준 후, 전처리


# In[6]:


total_keyword = law_keyword + test_keyword + Realtime_keyword


# In[74]:


f = open('keyword_list.txt','w')

for i in range(len(total_keyword)):
    f.write("%s " % total_keyword[i])

f.close()


# In[8]:


keyword_list = []


# 키워드 길이 1인 경우 제거
for i in range(len(total_keyword)):
    if len(total_keyword[i]) > 1 :
        keyword_list.append(total_keyword[i])


f = open('keyword_list2.txt','w')

for i in range(len(keyword_list)):
    f.write("%s " % keyword_list[i])

f.close()

