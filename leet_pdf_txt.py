
# coding: utf-8

# In[23]:


import os
import subprocess
import shutil
from os import listdir
from os.path import isfile, join

'''
for root, dirs, files, rootfd in os.fwalk(''):
'''

docLabels = []
docLabels = [f for f in listdir("/Users/yangseongjun/Downloads/추리논증") if f.endswith('.pdf')]

#os.mkdir('언어이해_텍스트')
os.mkdir('추리논증_텍스트')


# In[24]:


print(docLabels)


# In[25]:


# %run pdf2txt.py -o output2.xml T6-1-A4A-6-00412_R0.PDF -t xml
# python pdf2txt.py -o output.txt /Users/yangseongjun/Desktop/2019.pdf

command1 = 'python ' # 실행 명령어
#command2 = './' + 'pdf2txt.py ' # 실행 대상 파일 경로와 파일
command2 = 'pdf2txt.py ' # 실행 대상 파일 경로와 파일

count = 1 # 출력 파일을 만들기 위한 변수
file_list = [] # 생성된 xml 파일들의 이름을 저장하기 위한 리스트

for i in range(len(docLabels)):
    filename = '추리논증' + str(count) +'.txt' # output + 숫자 로 파일이름 생성
    file_list.append(filename)
    count = count + 1
    command3 = '-o ' + filename + ' ' # 출력 파일 형태 지정
    command4 = '/Users/yangseongjun/Downloads/추리논증/' + docLabels[i] # 파싱 대상 PDF 지정
    all_command = command1 + command2 + command3 + command4
    os.system(all_command)


# In[26]:


# 생성된 xml 파일들을 type1_html 폴더(자신이 임의로 생성한) 로 이동
for i in range(len(file_list)):
    shutil.move(file_list[i],'추리논증_텍스트')

