
# coding: utf-8

# In[22]:


import re
import shutil
from os import listdir
from os.path import isfile, join


#shutil.copytree('example', 'example2') # 원본 디렉토리를 바탕화면에 복사

docLabels = []
docLabels = [f for f in listdir("example2") if f.endswith('.txt')]


# In[24]:


def all_process(source):
    ex_index = 0
    ex_sentence = ""
    table_one_check = 0
    starting = 0
    
    f = open('example2/' + source,'r')
    lines = f.readlines()
    f.close()
    for i in range(len(lines)):
        sentence = lines[i];
        lines[i], table_one_check, starting = check_sentence(sentence, table_one_check, starting)
        if starting == 1:
            ex_index = i
            ex_sentence = sentence
        if starting == 2:
            lines[ex_index], table_one_check, starting = check_sentence(ex_sentence, table_one_check, starting)
        
       
    f = open('example2/' + source,'w')
    
    for sentence in lines:
        f.write(sentence)
    
    f.close()

def check_sentence(sentence, table_one_check, starting):
    
    title_regex = '(\d{1,2}\.){1,}\d*' # num.num.num ~~~ 감지
    sub_num_regex = '\d{1,2}\)' # ex) 1) 감지
    
    page_regex = ' Rev\.'
    Contents_regex = '\.{4,}' # ..... 감지
    
    table_cut_regex = '\S{1}\s{2,3}\S{1}' # 뛰어쓰기 두번, 세번 감지
    
    
    p_regex = '\S{1,}\.\s*$' # ex) go to home. 온점 감지
    comma_regex = '\w{1,},\s*$'
    colon_regex = '\w{1,}:\s*$'
    semicolon_regex = '\w{1,};\s*$'
    
    
    if re.search(table_cut_regex, sentence):
        count = re.findall(table_cut_regex, sentence)
        if len(count) > 1:
            sentence = "<table> " + re.sub(r'(?P<data>\S{1})\s{2,3}(?P<data2>\S{1})',r'\g<data> <|> \g<data2>',sentence)
            table_one_check = 0
            starting = 0
        else:
            if table_one_check == 1:
                sentence = "<table> " + re.sub(r'(?P<data>\S{1})\s{2,3}(?P<data2>\S{1})',r'\g<data> <|> \g<data2>',sentence)
                starting = starting + 1
                #print("haha")
            else:
                table_one_check = 1
                starting = 1

            if re.search(Contents_regex, sentence):
                sentence = "<Contents> " + sentence
            elif re.match(page_regex, sentence):
                sentence = "<Page_change> " + sentence
            elif re.search(p_regex, sentence):
                if re.match(title_regex, sentence):
                    sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
                elif re.match(sub_num_regex, sentence):
                    sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
                sentence = "<P_line> " + sentence
        
            elif re.search(comma_regex, sentence):
                if re.match(title_regex, sentence):
                    sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
                elif re.match(sub_num_regex, sentence):
                    sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
                sentence = "<Comma_line> " + sentence
        
            elif re.search(colon_regex, sentence):
                if re.match(title_regex, sentence):
                    sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
                elif re.match(sub_num_regex, sentence):
                    sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
                sentence = "<Colon_line> " + sentence
    
            elif re.search(semicolon_regex, sentence):
                if re.match(title_regex, sentence):
                    sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
                elif re.match(sub_num_regex, sentence):
                    sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
                sentence = "<Semicolon_line> " + sentence

            else :
                if re.match(title_regex, sentence):
                    sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
                elif re.match(sub_num_regex, sentence):
                    sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
        
                if re.match(r'\s{1}',sentence): 
                    pass
                else:
                    sentence = "<line> " + sentence         
                
    else:
        table_one_check = 0
        starting = 0
        if re.search(Contents_regex, sentence):
            sentence = "<Contents> " + sentence
        elif re.match(page_regex, sentence):
                sentence = "<Page_change> " + sentence
        elif re.search(p_regex, sentence):
            if re.match(title_regex, sentence):
                sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
            elif re.match(sub_num_regex, sentence):
                sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
            sentence = "<P_line> " + sentence
        
        elif re.search(comma_regex, sentence):
            if re.match(title_regex, sentence):
                sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
            elif re.match(sub_num_regex, sentence):
                sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
            sentence = "<Comma_line> " + sentence
        
        elif re.search(colon_regex, sentence):
            if re.match(title_regex, sentence):
                sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
            elif re.match(sub_num_regex, sentence):
                sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
            sentence = "<Colon_line> " + sentence
    
        elif re.search(semicolon_regex, sentence):
            if re.match(title_regex, sentence):
                sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
            elif re.match(sub_num_regex, sentence):
                sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
            sentence = "<Semicolon_line> " + sentence

        else :
            if re.match(title_regex, sentence):
                sentence = re.sub(r'(?P<title>(\d{1,2}\.){1,}\d*)',r'\g<title> <**>', sentence)
            elif re.match(sub_num_regex, sentence):
                sentence = re.sub(r'(?P<title>\d{1,2}\))',r'\g<title> <**>', sentence)
        
            if re.match(r'\s{1}',sentence): 
                pass
            else:
                sentence = "<line> " + sentence

    return sentence, table_one_check, starting


# In[25]:


if __name__ == '__main__':
    for i in range(len(docLabels)):
        all_process(docLabels[i])

