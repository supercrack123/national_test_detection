
# coding: utf-8

# In[1]:


import re
import shutil
from os import listdir
from os.path import isfile, join

from konlpy.corpus import kolaw


# In[2]:


# 헌법 조항 가져오기

fids = kolaw.fileids()
fobj = kolaw.open(fids[0])

law_data = ""
law_data = law_data + fobj.read()


# In[3]:


# 실시간 검색어 크롤링
import requests
from bs4 import BeautifulSoup


# Naver 실시간 검색어 API
req = requests.get('https://www.naver.com/')
soup = BeautifulSoup(req.content, 'html.parser')


title_list = soup.select('.ah_list .ah_k') 

f = open('Realtime_keyword.txt','a')
        
data = ""

for i, t in enumerate(title_list,1): #title_list에 숫자를 매긴다. (1부터시작함.)
    data = data + " " + t.get_text()
    f.write(t.get_text()+'\n')

    
    
# Naver 실시간 뉴스토픽 API    
html = requests.get('https://search.naver.com/search.naver?sm=tab_hty.top&where=post&query=ro0opf&oquery=a&tqi=Uc%2BgmspySDlsscImflGssssssid-378122').content
soup = BeautifulSoup(html, 'html.parser') 

title_list2 = [] 

for realtime in soup.find(class_='lst_realtime_srch _tab_area').find_all('li'): 
    tg2 = realtime.find(class_='tit') 
    title_list2.append(tg2.text)

for i, t in enumerate(title_list,1):
    data = data + " " + t.get_text()
    f.write(t.get_text()+'\n')

for i, t in enumerate(title_list,11):
    data = data + " " + t.get_text()
    f.write(t.get_text()+'\n')

    
# Daum 실시간 검색어 API    
html = requests.get('https://www.daum.net/').text
soup = BeautifulSoup(html, 'html.parser')

title_list = soup.select('.hotissue_mini a[class*=link_issue]')
ranking = soup.select('.list_mini span[class*=ir_wa]')

for rank,title in zip(ranking , title_list):
    r = ''.join(rank)
    t = ''.join(title)
    f.write(t + '\n')

f.close()


# In[180]:


from konlpy.tag import Twitter
from konlpy.utils import pprint

twitter = Twitter()

# twitter.morphs()
# twitter.nouns()
# twitter.pos()

total_set = twitter.nouns(law_data)


# In[181]:


import re
pattern = re.compile(r'\n')

f = open('Realtime_keyword.txt','r')
lines = f.readlines()
f.close()


# In[191]:


for i in range(len(lines)):
    lines[i] = re.sub(pattern, '', lines[i]);
    total_set.append(lines[i]);

def double_check(text):
    return [x for i, x in enumerate(text) if x not in text[:i]];

total_set = double_check(total_set)

trend_word = []

# 키워드 1개 짜리 제거
for i in range(len(total_set)):
    if len(total_set[i]) > 1 :
        trend_word.append(total_set[i])
        
# trend_word -> 헌법 + 실시간 검색어 키워드 전처리


# In[192]:


docLabels = []
docLabels = [f for f in listdir("leet/national_test_detection/언어이해_텍스트") if f.endswith('.txt')]
test1 = []
test1_data = ""

docLabels2 = []
docLabels2 = [f for f in listdir("leet/national_test_detection/추리논증_텍스트") if f.endswith('.txt')]
test2 = []
test2_data = ""


# In[193]:


def all_process(source):
    f = open('leet/national_test_detection/' + source,'r')
    lines = f.readlines()
    f.close()
    return lines;
    
    
for i in range(len(docLabels)):
    test1 = test1 + all_process('언어이해_텍스트/' + docLabels[i])

test1_data = "".join(test1)

for i in range(len(docLabels2)):
    test2 = test2 + all_process('추리논증_텍스트/' + docLabels2[i])

test2_data = "".join(test2)


# In[194]:


# twitter.morphs()
# twitter.nouns()
# twitter.pos()

total_result = []

test_result1 = twitter.nouns(test1_data)

test_result2 = twitter.nouns(test2_data)

for i in range(len(test_result1)):
    if len(test_result1[i]) > 1 :
        total_result.append(test_result1[i]);
        
for i in range(len(test_result2)):
    if len(test_result2[i]) > 1:
        total_result.append(test_result2[i]);
        
total_result = list(set(total_result))


# In[195]:


total_result = total_result + trend_word


# In[197]:


f = open('total_result_keyword.txt','w')

for i in range(len(total_result)):
    f.write(total_result[i] + '\n')

f.close()

