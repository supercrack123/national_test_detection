
# coding: utf-8

# In[25]:


from navernewscrawler.naver_crawler import get_news, get_dates, output
import re

pattern = re.compile(r'\n')


# In[26]:


f = open('total_result_keyword.txt','r')
lines = f.readlines()
f.close()

total_set = []

for i in range(len(lines)):
    lines[i] = re.sub(pattern, '', lines[i]);
    total_set.append(lines[i]);


# In[51]:


query = total_set[14] + " 자유" # 키워드 매치

news_url = "https://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=102&oid=001&aid=0010706739"
news_result = get_news(news_url)
search_result = output(query=query, page=1, max_page=10, start_date="2019-1-1",end_date="2019-04-30")


# In[52]:


final_result = []

if len(search_result) > 0 :
    for i in range(len(search_result)) :
        final_result.append(search_result[i]['text']);
        


# In[53]:


print(final_result)

