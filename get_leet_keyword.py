
# coding: utf-8

# In[10]:


import re
import shutil
from os import listdir
from os.path import isfile, join


#shutil.copytree('example', 'example2') # 원본 디렉토리를 바탕화면에 복사

docLabels = []
docLabels = [f for f in listdir("/Users/yangseongjun/Downloads/추리논증_텍스트") if f.endswith('.txt')]


# In[11]:


print(docLabels)


# In[ ]:


def del_special(sentence): # 태핑, 강제개행 등 불필요한 글자들을 제거하는 함수부
    sentence = re.sub('\t', '', sentence) # 태핑 제거
    sentence = re.sub('\n', '', sentence) # 강제개행 제거
    sentence = " ".join(sentence.split()) # 뛰어 쓰기 제거
    
    return sentence

